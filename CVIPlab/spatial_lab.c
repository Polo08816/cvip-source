/*
** include header files
*/

#include "CVIPtoolkit.h"
#include "CVIPconvert.h"
#include "CVIPdef.h"
#include "CVIPimage.h"
#include "CVIPlab.h"

/* 
** The following function will compare the actual gray level of the input image 
** with the threshold limit. If the gray-level value is greater than the 
** threshold limit then the gray level is set to 255 (WHITE_LAB) else to 
** 0 (BLACK_LAB). Note that the '_LAB' or '_lab' is appended to names used
** in CVIPlab to avoid naming conflicts with existing constant and function 
** (e.g. threshold_lab) names.
*/

#define 	WHITE_LAB	255
#define		BLACK_LAB	0


Image *spatial_lab(Image *inputImage, unsigned int spatialchoice){

	Image *duplicateImage;
    byte 	**image; 	/* 2-d matrix data pointer */
	byte	**imageOriginal; /* 2-d matrix data pointer of original image */
    unsigned int 		r,		/* row index */
			c, 		/* column index */
			bands;		/* band index */
			
    unsigned int 	no_of_rows,	/* number of rows in image */
			no_of_cols,	/* number of columns in image */
			no_of_bands;	/* number of image bands */

	int n, sum;

	signed int mean1[3][3] = {
				{1,1,1},
				{1,1,1},
				{1,1,1}
			};

	signed int mean2[3][3] = {
				{1,1,1},
				{1,2,1},
				{1,1,1}
			};

	signed int mean3[3][3] = {
				{1,2,1},
				{2,4,2},
				{1,2,1}
			};

	duplicateImage = duplicate_Image(inputImage);

    /*
    ** Gets the number of image bands (planes)
    */
    no_of_bands = getNoOfBands_Image(duplicateImage);
	print_CVIP("\nNumber of Bands %u\n", no_of_bands);

    /* 
    ** Gets the number of rows in the input image  
    */
    no_of_rows =  getNoOfRows_Image(duplicateImage);

    /* 
    ** Gets the number of columns in the input image  
    */
    no_of_cols =  getNoOfCols_Image(duplicateImage);

	
	/* begin processing image based on selection */

	switch(spatialchoice){


		case 1:

			

			break;

		case 2:

			


			break;

		case 3:

			

			break;

	}

    /* 
    ** Compares the pixel value at the location (r,c)
    ** with the threshold value. If it is greater than 
    ** the threshold value it writes 255 at the location
    ** else it writes 0. Note thta this assumes the input
    ** image is of data type BYTE.
    */
    for(bands=0; bands < no_of_bands; bands++) {
	/*
	** reference each band of image data in 2-d matrix form;
	** which is used for reading and writing the pixel values
	*/
  	image = getData_Image(duplicateImage, bands);
	imageOriginal = getData_Image(inputImage, bands);

  	for(r=0; r < no_of_rows; r++) {
     	    for(c=0; c < no_of_cols; c++) {
			/*	if(image[r][c] > (byte) threshval)
            	    image[r][c] = WHITE_LAB;
	  			else 
	     			image[r][c] = BLACK_LAB;*/
            }
     	}
    }
	

    return duplicateImage;
}